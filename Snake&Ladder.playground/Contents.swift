//Snake & Ladders!!
//Size: 6 x 6

import UIKit

//a welcome message
println("Let's play Snake & Ladders!")

//creating and populating array with zeros
var map: [Int] = [Int](count:36, repeatedValue:0)
var currentBox: Int = 0
let winBox: Int = 36


//create multiple players
let numberOfPlayers: Int = 2
var player: [Int] = [Int]()

for var playerID = 0; playerID < numberOfPlayers;playerID++
{
    player.append(0)
}

//hardcoding the snakes and ladders
//3 snakes & 3 ladders

//ladders
map[10] = 4
map[21] = 3
map[13] = 10

//snakes
map[23] = -6
map[16] = -2
map[25] = -3

//dice roll function

func diceRoll(sides: UInt32, numOfDie: Int) -> Int {
    var total : Int = 0
    //random integer between 1 - 6
    for var counter = 0; counter < numOfDie; counter++
    {
       var rand = Int(arc4random_uniform(sides)+1)
       total = total + rand
    }
   
    return total
}


var endGame = false
while endGame == false
{

    for var playerID = 0; playerID < numberOfPlayers;playerID++
    {
        var diceValue = diceRoll(6,1)
        
        player[playerID] += diceValue
        
        if player[playerID] > winBox
        {
            player[playerID] = 36 - (player[playerID] - 36)
        }
        else
        {
            player[playerID] = player[playerID] + map[player[playerID]-1]
        }
        
        println("The player'\(playerID+1)' position is at \(player[playerID])");
        
        if player[playerID] == winBox
        {
            endGame = true
            break
        }
      
    }
      println()
}


println("Endgame")






